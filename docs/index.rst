===============================================================================
Core Library
===============================================================================

Repository: ``git clone https://gitlab.com/AcceleratXR/composerjs/composer-core.git``

A collection of common utilities and core functionality for composerjs applications.

.. toctree::
   :caption: Project Info

   reference/README
   
.. toctree::
   :caption: API Reference
   
   reference/globals