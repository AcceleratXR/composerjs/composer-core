[@composer-js/core](README.md) › [Globals](globals.md)

# @composer-js/core

# Composer: Core Library

[![pipeline status](https://gitlab.com/AcceleratXR/composerjs/composer-core/badges/master/pipeline.svg)](https://gitlab.com/AcceleratXR/composerjs/composer-core/-/commits/master)
[![coverage report](https://gitlab.com/AcceleratXR/composerjs/composer-core/badges/master/coverage.svg)](https://gitlab.com/AcceleratXR/composerjs/composer-core/-/commits/master)

A set of common utilities shared amongst all Composer based applications and servers.

## Features
* Thread-pool Manager
* JSON Web Token utilities
* String utilities
* User authentication utilities

## Yarn
```
yarn add @composer-js/core
```

## NPM
```
npm i @composer-js/core
```
