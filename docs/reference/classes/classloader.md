[@composer-js/core](../README.md) › [Globals](../globals.md) › [ClassLoader](classloader.md)

# Class: ClassLoader

The ClassLoader provides a container for loading and managing TypeScript and JavaScript classes from disk. Loaded
classes are referenced by name and package. The package is derived from the folder the class was loaded from
relative to the root. It is expected that each file defines their class as the only export and the name of the file
is the same as the class itself.

For example, given the class fully qualified class name `com.company.MyClass` would load a TypeScript or JavaScript
file from disk. In the case of a TypeScript class, the file `com/company/MyClass.ts` is loaded whose contents
would look like the following.

```javascript
class MyClass {
    // ...
}

export default MyClass;
```

In the case of a JavaScript class, the file `com/company/MyClass.js` is loaded whose contents would look like the
following.

```javascript
`use strict`;

class MyClass {
    // ...
}

modules.exports = MyClass;
```

**`author`** Jean-Philippe Steinmetz

## Hierarchy

* **ClassLoader**

## Index

### Constructors

* [constructor](classloader.md#constructor)

### Properties

* [classes](classloader.md#protected-classes)
* [includeJavaScript](classloader.md#protected-includejavascript)
* [includeTypeScript](classloader.md#protected-includetypescript)
* [rootDir](classloader.md#protected-rootdir)

### Methods

* [GetClass](classloader.md#getclass)
* [GetClasses](classloader.md#getclasses)
* [HasClass](classloader.md#hasclass)
* [Load](classloader.md#load)

## Constructors

###  constructor

\+ **new ClassLoader**(`rootDir`: string, `includeJavaScript`: boolean, `includeTypeScript`: boolean): *[ClassLoader](classloader.md)*

Defined in ClassLoader.ts:48

Creates a new instance of `ClassLoader` with the specified defaults.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`rootDir` | string | - | The root directory to load all classes from. |
`includeJavaScript` | boolean | true | Set to `true` to load all TypeScript classes from the given `rootDir`, otherwise set                          to `false. |
`includeTypeScript` | boolean | true | Set to `true` to load all JavaScript classes from the given `rootDir`, otherwise set                          to `false.  |

**Returns:** *[ClassLoader](classloader.md)*

## Properties

### `Protected` classes

• **classes**: *Map‹string, any›* =  new Map<string, any>()

Defined in ClassLoader.ts:42

The map containnig all loaded classes.

___

### `Protected` includeJavaScript

• **includeJavaScript**: *boolean* = true

Defined in ClassLoader.ts:46

Indicates if JavaScript classes should be loaded.

___

### `Protected` includeTypeScript

• **includeTypeScript**: *boolean* = true

Defined in ClassLoader.ts:44

Indicates if TypeScript classes should be loaded.

___

### `Protected` rootDir

• **rootDir**: *string* = "."

Defined in ClassLoader.ts:48

The path to the root directory containing all classes on disk.

## Methods

###  GetClass

▸ **GetClass**(`fqn`: string): *any | undefined*

Defined in ClassLoader.ts:71

Returns the class with the specified fully qualified name.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`fqn` | string | The fully qualified name of the class to return. |

**Returns:** *any | undefined*

The class definition for the given fully qualified name if found, otherwise `undefined`.

___

###  GetClasses

▸ **GetClasses**(): *Map‹string, any›*

Defined in ClassLoader.ts:78

Returns the map containing all classes that have been loaded.

**Returns:** *Map‹string, any›*

___

###  HasClass

▸ **HasClass**(`fqn`: string): *any | undefined*

Defined in ClassLoader.ts:88

Returns `true` if a class exists with the specified fully qualified name.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`fqn` | string | The fully qualified name of the class to search. |

**Returns:** *any | undefined*

`true` if a class definition exists for the given fully qualified name, otherwise `false`.

___

###  Load

▸ **Load**(`dir`: string): *Promise‹void›*

Defined in ClassLoader.ts:98

Loads all TypeScript classes contained in the directory specified. The folder must be a child
directory to the `rootDir` parameter passed in to the constructor.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`dir` | string | "" | The directory, relative to `rootDir`, containing TypeScript classes to load.  |

**Returns:** *Promise‹void›*
