[@composer-js/core](../README.md) › [Globals](../globals.md) › [FileUtils](fileutils.md)

# Class: FileUtils

Utility functions for working with files.

**`author`** Jean-Philippe Steinmetz

## Hierarchy

* **FileUtils**

## Index

### Methods

* [copyBinaryFile](fileutils.md#static-copybinaryfile)
* [copyDirectory](fileutils.md#static-copydirectory)
* [copyFile](fileutils.md#static-copyfile)
* [writeFile](fileutils.md#static-writefile)

## Methods

### `Static` copyBinaryFile

▸ **copyBinaryFile**(`srcPath`: string, `outPath`: string, `variables`: any): *Promise‹void›*

Defined in FileUtils.ts:162

Generates a copy of the source file at the desired output destination using binary copy mode.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`srcPath` | string | - | The source file to copy. |
`outPath` | string | - | The destination file to generate. |
`variables` | any |  {} | The map of variable names to values to swap. Applies to outPath only.  |

**Returns:** *Promise‹void›*

___

### `Static` copyDirectory

▸ **copyDirectory**(`srcPath`: string, `outPath`: string, `vars`: any, `excludeFilters`: Array‹string›, `binaryFilters`: Array‹string›, `force`: boolean): *Promise‹void›*

Defined in FileUtils.ts:190

Performs a deep copy of a directory tree at the given srcPath to the specified output directory. Performs
template replacement for all variables given and skips any files in the specified filter.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`srcPath` | string | - | The path to the source directory to copy files from. |
`outPath` | string | - | The path to the destination directory to copy files to. |
`vars` | any |  {} | The map of template variables to perform replacement on. |
`excludeFilters` | Array‹string› |  [] | The list of file extension filters to exclude during the copy process. |
`binaryFilters` | Array‹string› |  [] | The list of file extension filters to copy as binary only. |
`force` | boolean | false | Set to `true` to force writing over any existing files.  |

**Returns:** *Promise‹void›*

___

### `Static` copyFile

▸ **copyFile**(`srcPath`: string, `outPath`: string, `variables`: any, `overwrite`: boolean): *Promise‹void›*

Defined in FileUtils.ts:126

Generates a copy of the source file at the desired output destination and performs a swap of all values of the
variables specified.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`srcPath` | string | - | The source file to copy. |
`outPath` | string | - | The destination file to generate. |
`variables` | any |  {} | The map of variable names to values to swap.  |
`overwrite` | boolean | false | - |

**Returns:** *Promise‹void›*

___

### `Static` writeFile

▸ **writeFile**(`srcPath`: string, `outPath`: string, `contents`: any, `overwrite`: boolean): *Promise‹void›*

Defined in FileUtils.ts:31

Attempts to write the provided contents to the file path given. If a file already exists the user is prompted to
allow the file to be overwritten or merged. In the case of a merge, srcPath is used as a baseline in order to
perform a 3-way merge.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`srcPath` | string | - | The baseline template file to use during a merge. |
`outPath` | string | - | The destination file path to be written. |
`contents` | any | - | The contents of the file to write. |
`overwrite` | boolean | false | Set to `true` to overwite the file and not perform a merge.  |

**Returns:** *Promise‹void›*
