[@composer-js/core](../README.md) › [Globals](../globals.md) › [JWTUtils](jwtutils.md)

# Class: JWTUtils

Utility class for working with Json Web Token (JWT) authentication tokens.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* **JWTUtils**

## Index

### Methods

* [createToken](jwtutils.md#static-createtoken)
* [decodeToken](jwtutils.md#static-decodetoken)

## Methods

### `Static` createToken

▸ **createToken**(`config`: [JWTUtilsConfig](../interfaces/jwtutilsconfig.md), `user`: [JWTUser](../interfaces/jwtuser.md)): *string*

Defined in JWTUtils.ts:148

Generates a new JWT token for the given config and user object. The user object must be a valid AcceleratXR
user.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`config` | [JWTUtilsConfig](../interfaces/jwtutilsconfig.md) | The JWT configuration to use when generating the token. |
`user` | [JWTUser](../interfaces/jwtuser.md) | The user to encode into the token's payload.  |

**Returns:** *string*

___

### `Static` decodeToken

▸ **decodeToken**(`config`: [JWTUtilsConfig](../interfaces/jwtutilsconfig.md), `token`: string): *[JWTUser](../interfaces/jwtuser.md)*

Defined in JWTUtils.ts:197

Decodes the given JWT authentication token using the provided configuration. If the token is not valid an
error is thrown with the reason. Returns the encoded user object payload upon success.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`config` | [JWTUtilsConfig](../interfaces/jwtutilsconfig.md) | The JWT configuration to use when validating the token. |
`token` | string | The JWT token to validate. |

**Returns:** *[JWTUser](../interfaces/jwtuser.md)*

The user object encoded in the token's payload.
