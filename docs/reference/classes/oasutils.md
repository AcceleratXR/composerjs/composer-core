[@composer-js/core](../README.md) › [Globals](../globals.md) › [OASUtils](oasutils.md)

# Class: OASUtils

## Hierarchy

* **OASUtils**

## Index

### Methods

* [getDatastore](oasutils.md#static-getdatastore)
* [getObject](oasutils.md#static-getobject)
* [getResponse](oasutils.md#static-getresponse)
* [getResponseContent](oasutils.md#static-getresponsecontent)
* [getSchema](oasutils.md#static-getschema)
* [getTypeInfo](oasutils.md#static-gettypeinfo)
* [loadSpec](oasutils.md#static-loadspec)

## Methods

### `Static` getDatastore

▸ **getDatastore**(`spec`: any, `name`: string): *any*

Defined in OASUtils.ts:20

Gets the datastore definition with the specified name.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`spec` | any | The OpenAPI specification to search. |
`name` | string | The name of the datastore to retrieve. |

**Returns:** *any*

The definition for the datastore with the given name if found, otherwise `undefined`.

___

### `Static` getObject

▸ **getObject**(`spec`: any, `path`: string): *any*

Defined in OASUtils.ts:37

Gets the specification object at the specified path.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`spec` | any | The OpenAPI specification to reference. |
`path` | string | The path of the object to retrieve. |

**Returns:** *any*

The object at the specified path if found, otherwise `undefined`.

___

### `Static` getResponse

▸ **getResponse**(`obj`: any): *any*

Defined in OASUtils.ts:64

Returns the first available response object for a 2XX response as defined by the provided Operation schema object.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`obj` | any | The Operation schema object to search.  |

**Returns:** *any*

___

### `Static` getResponseContent

▸ **getResponseContent**(`obj`: any): *any*

Defined in OASUtils.ts:83

Returns the first available response content object for a 2XX response as defined by the provided Operation schema object.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`obj` | any | The Operation schema object to search.  |

**Returns:** *any*

___

### `Static` getSchema

▸ **getSchema**(`spec`: any, `name`: string): *any*

Defined in OASUtils.ts:95

Retrieves the schema definition with the given name.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`spec` | any | The OpenAPI specification object to reference. |
`name` | string | The name of the schema to retrieve. |

**Returns:** *any*

The schema definition with the given name.

___

### `Static` getTypeInfo

▸ **getTypeInfo**(`schemaDef`: any, `spec`: any, `convertDataType`: Function): *any*

Defined in OASUtils.ts:118

Extracts the type information for a given schema Object definition.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`schemaDef` | any | The schema definition object to extract type information from. |
`spec` | any | The entire OpenAPI specification object. |
`convertDataType` | Function | The function that converts OpenAPI Specification types to native types. |

**Returns:** *any*

A tuple containing the type, subType and subSchemaRef information.

___

### `Static` loadSpec

▸ **loadSpec**(`file`: string): *Promise‹any›*

Defined in OASUtils.ts:173

Attempts to load the Open API specification at the given path or URL.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`file` | string | The path or URL of the OpenAPI Specification file to load. |

**Returns:** *Promise‹any›*

A promise whose result will be the loaded OpenAPI Specification as an object.
