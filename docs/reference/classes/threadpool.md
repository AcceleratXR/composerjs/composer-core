[@composer-js/core](../README.md) › [Globals](../globals.md) › [ThreadPool](threadpool.md)

# Class: ThreadPool

The `ThreadPool` class implements a method of managing a pool of execution threads that can be used for parallel
code execution. `Job`s are submitted to the pool and assigned to an available worker thread for execution.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* **ThreadPool**

## Index

### Constructors

* [constructor](threadpool.md#constructor)

### Properties

* [callbacks](threadpool.md#private-callbacks)
* [lastThread](threadpool.md#private-lastthread)
* [maxThreads](threadpool.md#private-maxthreads)
* [restartOnExit](threadpool.md#private-restartonexit)
* [shutdown](threadpool.md#private-shutdown)
* [workers](threadpool.md#workers)

### Accessors

* [max](threadpool.md#max)
* [size](threadpool.md#size)

### Methods

* [createWorker](threadpool.md#private-createworker)
* [on](threadpool.md#on)
* [send](threadpool.md#send)
* [sendAll](threadpool.md#sendall)
* [start](threadpool.md#start)
* [stop](threadpool.md#stop)

## Constructors

###  constructor

\+ **new ThreadPool**(`max`: number, `restartOnExit`: boolean): *[ThreadPool](threadpool.md)*

Defined in threads/ThreadPool.ts:39

Creates a new `ThreadPool` instance with the specified defaults.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`max` | number | 0 | The maximum number of threads to create. Default is `os.cpus().length`. |
`restartOnExit` | boolean | true | Set to `true` to restart a worker thread on exit, otherwise set to `false`. Default is `true`.  |

**Returns:** *[ThreadPool](threadpool.md)*

## Properties

### `Private` callbacks

• **callbacks**: *Map‹string, Array‹Function››*

Defined in threads/ThreadPool.ts:15

The map of event types to a list of callback functions.

___

### `Private` lastThread

• **lastThread**: *number*

Defined in threads/ThreadPool.ts:17

The index of the last worker that was assigned work.

___

### `Private` maxThreads

• **maxThreads**: *number*

Defined in threads/ThreadPool.ts:19

The maximum number of threads allowed.

___

### `Private` restartOnExit

• **restartOnExit**: *boolean*

Defined in threads/ThreadPool.ts:21

Indicates if a worker thread should automatically be restarted on exit.

___

### `Private` shutdown

• **shutdown**: *boolean*

Defined in threads/ThreadPool.ts:25

Used to indicate that the pool is shutting down.

___

###  workers

• **workers**: *Array‹Worker›*

Defined in threads/ThreadPool.ts:23

The list of active worker threads.

## Accessors

###  max

• **get max**(): *number*

Defined in threads/ThreadPool.ts:30

The maximum number of threads that can be created by the pool.

**Returns:** *number*

___

###  size

• **get size**(): *number*

Defined in threads/ThreadPool.ts:37

The number of active threads in the pool.

**Returns:** *number*

## Methods

### `Private` createWorker

▸ **createWorker**(`idx`: number, `script`: string, `args`: any): *Promise‹Worker›*

Defined in threads/ThreadPool.ts:59

**Parameters:**

Name | Type |
------ | ------ |
`idx` | number |
`script` | string |
`args` | any |

**Returns:** *Promise‹Worker›*

___

###  on

▸ **on**(`type`: string, `func`: Function): *void*

Defined in threads/ThreadPool.ts:153

Registers a new callback function to be notified when the given event type is fired.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`type` | string | The event type to be notified of. Possible values are: `error`, `exit` and `message`. |
`func` | Function | The callback function to register.  |

**Returns:** *void*

___

###  send

▸ **send**(`msg`: any): *void*

Defined in threads/ThreadPool.ts:166

Sends the provided message to the next available worker thread. Messages are sent in a round-robin order.

**Parameters:**

Name | Type |
------ | ------ |
`msg` | any |

**Returns:** *void*

___

###  sendAll

▸ **sendAll**(`msg`: any): *void*

Defined in threads/ThreadPool.ts:181

Sends the provided message to all worker threads in the pool.

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`msg` | any | The message to send to all workers.  |

**Returns:** *void*

___

###  start

▸ **start**(`script`: string, `args`: any, `num`: number): *Promise‹void›*

Defined in threads/ThreadPool.ts:109

Initializes the thread pool with the initial worker threads and begins execution.

**Parameters:**

Name | Type | Default | Description |
------ | ------ | ------ | ------ |
`script` | string | - | The script file to execute in the worker thread. |
`args` | any | - | The arguments to pass to the worker thread. |
`num` | number |  this.max | The number of initial threads to create, cannot be greater than max. Default is `max`.  |

**Returns:** *Promise‹void›*

___

###  stop

▸ **stop**(): *Promise‹void›*

Defined in threads/ThreadPool.ts:133

Stops all running thread executions.

**Returns:** *Promise‹void›*
