[@composer-js/core](../README.md) › [Globals](../globals.md) › [JWTUtilsConfig](jwtutilsconfig.md)

# Interface: JWTUtilsConfig

Describes the configuration options to be used with `JWTUtils` functions.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* **JWTUtilsConfig**

## Index

### Properties

* [options](jwtutilsconfig.md#optional-options)
* [password](jwtutilsconfig.md#password)
* [payload](jwtutilsconfig.md#optional-payload)

## Properties

### `Optional` options

• **options**? : *SignOptions | VerifyOptions*

Defined in JWTUtils.ts:127

The options to use when performing JWT signing or verification.

___

###  password

• **password**: *string*

Defined in JWTUtils.ts:122

The password to use for signing and verifying JWT tokens.

___

### `Optional` payload

• **payload**? : *[JWTUtilsPayloadOptions](jwtutilspayloadoptions.md) | [JWTUtilsPayloadKeyOptions](jwtutilspayloadkeyoptions.md) | [JWTUtilsPayloadPasswordOptions](jwtutilspayloadpasswordoptions.md)*

Defined in JWTUtils.ts:132

The options that determine how JWT token payloads will be handled.
