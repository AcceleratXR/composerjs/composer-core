[@composer-js/core](../README.md) › [Globals](../globals.md) › [JWTUtilsPayloadPasswordOptions](jwtutilspayloadpasswordoptions.md)

# Interface: JWTUtilsPayloadPasswordOptions

Describes the configuration options to be used with the `JWTUtilsConfig.payload` property when performing password
based encryption.

**`author`** Jean-Philippe Steinmetz <info@acceleratxr.com>

## Hierarchy

* [JWTUtilsPayloadOptions](jwtutilspayloadoptions.md)

  ↳ **JWTUtilsPayloadPasswordOptions**

## Index

### Properties

* [algorithm](jwtutilspayloadpasswordoptions.md#algorithm)
* [compress](jwtutilspayloadpasswordoptions.md#compress)
* [encrypt](jwtutilspayloadpasswordoptions.md#encrypt)
* [iv](jwtutilspayloadpasswordoptions.md#iv)
* [password](jwtutilspayloadpasswordoptions.md#password)

## Properties

###  algorithm

• **algorithm**: *string*

Defined in JWTUtils.ts:82

The cryptographic cipher algorithm to use during encryption/decryption of a JWT token payload.

___

###  compress

• **compress**: *boolean*

*Inherited from [JWTUtilsPayloadOptions](jwtutilspayloadoptions.md).[compress](jwtutilspayloadoptions.md#compress)*

Defined in JWTUtils.ts:64

Set to `true` to enable payload compression, otherwise set to `false`.

___

###  encrypt

• **encrypt**: *boolean*

*Inherited from [JWTUtilsPayloadOptions](jwtutilspayloadoptions.md).[encrypt](jwtutilspayloadoptions.md#encrypt)*

Defined in JWTUtils.ts:69

Set to `true` to indicate that the JWT token payload is encrypted, otherwise set to `false`.

___

###  iv

• **iv**: *Buffer*

Defined in JWTUtils.ts:87

The initialization vector to use during encryption and decryption.

___

###  password

• **password**: *string*

Defined in JWTUtils.ts:92

The password to use when encrypting or decrypting JWT token payloads.
