"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Copyright (C) AcceleratXR, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var child_process = require('child_process');
var fs = require('fs');
var path = require('path');
var mkdirp = require('mkdirp');
var readline = require('readline');
var StringUtils = require('./StringUtils');
var util = require('util');
var mkdirpAsync = util.promisify(mkdirp);
var logger_1 = __importDefault(require("./logger"));
/**
 * Utility functions for working with files.
 *
 * @author Jean-Philippe Steinmetz
 */
var FileUtils = /** @class */ (function () {
    function FileUtils() {
    }
    /**
     * Attempts to write the provided contents to the file path given. If a file already exists the user is prompted to
     * allow the file to be overwritten or merged. In the case of a merge, srcPath is used as a baseline in order to
     * perform a 3-way merge.
     *
     * @param {any} srcPath The baseline template file to use during a merge.
     * @param {any} outPath The destination file path to be written.
     * @param {any} contents The contents of the file to write.
     */
    FileUtils.prototype.writeFile = function (srcPath, outPath, contents) {
        return __awaiter(this, void 0, void 0, function () {
            var srcPathFull, outPathFull, fileExists, overwrite, outDirPath, invalidResponse, response, tmpPath, mergedPath, success, _a, stdout, stderr, err_1, merged;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        srcPathFull = path.resolve(srcPath);
                        outPathFull = path.resolve(outPath);
                        fileExists = fs.existsSync(outPathFull);
                        overwrite = false;
                        outDirPath = path.dirname(outPath);
                        if (!!fs.existsSync(outDirPath)) return [3 /*break*/, 2];
                        return [4 /*yield*/, mkdirpAsync(outDirPath)];
                    case 1:
                        _b.sent();
                        _b.label = 2;
                    case 2:
                        if (!fileExists) return [3 /*break*/, 5];
                        invalidResponse = false;
                        _b.label = 3;
                    case 3:
                        if (!invalidResponse) return [3 /*break*/, 5];
                        return [4 /*yield*/, new Promise(function (resolve, reject) {
                                try {
                                    readline('Overwrite existing file: ' + outPath + '? [Y]es, [N]o, [M]erge: ', function (answer) {
                                        resolve(answer);
                                    });
                                }
                                catch (err) {
                                    reject(err);
                                }
                            })];
                    case 4:
                        response = _b.sent();
                        response = response.toLocaleLowerCase();
                        if (response.length > 0) {
                            if (response[0] === 'y' || response[0] === 'n' || response[0] === 'm') {
                                if (response[0] === 'n') {
                                    // Immediately exit, no point in continuing
                                    return [2 /*return*/];
                                }
                                overwrite = response[0] === 'y';
                            }
                            else {
                                logger_1.default.info("Invalid input: " + response);
                            }
                        }
                        else {
                            logger_1.default.info("Invalid input: " + response);
                        }
                        return [3 /*break*/, 3];
                    case 5:
                        if (!(!fileExists || overwrite)) return [3 /*break*/, 6];
                        // Write the final output to disk
                        logger_1.default.info("Writing: " + outPathFull);
                        fs.writeFileSync(outPathFull, contents);
                        return [3 /*break*/, 11];
                    case 6:
                        tmpPath = path.resolve(outPathFull + '.new');
                        fs.writeFileSync(tmpPath, contents);
                        mergedPath = path.resolve(outPathFull + '.merged');
                        success = true;
                        _b.label = 7;
                    case 7:
                        _b.trys.push([7, 9, , 10]);
                        return [4 /*yield*/, child_process.exec('kdiff3 ' + srcPathFull + ' ' + outPathFull + ' ' + tmpPath + ' -m --cs "ShowInfoDialogs=0" -o ' + mergedPath)];
                    case 8:
                        _a = _b.sent(), stdout = _a.stdout, stderr = _a.stderr;
                        return [3 /*break*/, 10];
                    case 9:
                        err_1 = _b.sent();
                        success = false;
                        return [3 /*break*/, 10];
                    case 10:
                        merged = fs.readFileSync(mergedPath, 'utf-8');
                        merged = merged.replace(new RegExp('^// Last Generated.*$', 'g'), '// Last Generated: ' + new Date());
                        // Write the merged copy to the final destination and clean up temporary files
                        logger_1.default.info("Writing: " + outPathFull);
                        fs.writeFileSync(outPathFull, merged);
                        fs.unlinkSync(mergedPath);
                        fs.unlinkSync(tmpPath);
                        _b.label = 11;
                    case 11: return [2 /*return*/];
                }
            });
        });
    };
    ;
    /**
     * Generates a copy of the source file at the desired output destination and performs a swap of all values of the
     * variables specified.
     *
     * @param {string} srcPath The source file to copy.
     * @param {string} outPath The destination file to generate.
     * @param {object} variables The map of variable names to values to swap.
     */
    FileUtils.prototype.copyFile = function (srcPath, outPath, variables) {
        return __awaiter(this, void 0, void 0, function () {
            var srcPathFull, outDirPath, template, output, outPathFinal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        srcPathFull = path.resolve(srcPath);
                        if (!fs.existsSync(srcPathFull)) {
                            throw new Error("File does not exist: " + srcPathFull);
                        }
                        outDirPath = path.dirname(outPath);
                        if (!!fs.existsSync(outDirPath)) return [3 /*break*/, 2];
                        return [4 /*yield*/, mkdirpAsync(outDirPath)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        template = fs.readFileSync(srcPathFull, 'utf-8');
                        if (template) {
                            output = StringUtils.findAndReplace(template, variables);
                            outPathFinal = StringUtils.findAndReplace(outPath, variables);
                            logger_1.default.info("Writing: " + outPathFinal);
                            this.writeFile(srcPath, outPathFinal, output);
                        }
                        else {
                            throw new Error("Failed to read file: " + srcPathFull);
                        }
                        return [2 /*return*/];
                }
            });
        });
    };
    ;
    /**
     * Generates a copy of the source file at the desired output destination using binary copy mode.
     *
     * @param {string} srcPath The source file to copy.
     * @param {string} outPath The destination file to generate.
     * @param {any} variables The map of variable names to values to swap. Applies to outPath only.
     */
    FileUtils.prototype.copyBinaryFile = function (srcPath, outPath, variables) {
        return __awaiter(this, void 0, void 0, function () {
            var srcPathFull, outDirPath, outPathFinal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        srcPathFull = path.resolve(srcPath);
                        if (!fs.existsSync(srcPathFull)) {
                            throw new Error("File does not exist: " + srcPathFull);
                        }
                        outDirPath = path.dirname(outPath);
                        if (!!fs.existsSync(outDirPath)) return [3 /*break*/, 2];
                        return [4 /*yield*/, mkdirpAsync(outDirPath)];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        outPathFinal = StringUtils.findAndReplace(outPath, variables);
                        fs.copyFileSync(srcPathFull, outPathFinal);
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Performs a deep copy of a directory tree at the given srcPath to the specified output directory. Performs
     * template replacement for all variables given and skips any files in the specified filter.
     *
     * @param {string} srcPath The path to the source directory to copy files from.
     * @param {string} outPath The path to the destination directory to copy files to.
     * @param {map} vars The map of template variables to perform replacement on.
     * @param {array} excludeFilters The list of file extension filters to exclude during the copy process.
     * @param {array} binaryFilters The list of file extension filters to copy as binary only.
     */
    FileUtils.prototype.copyDirectory = function (srcPath, outPath, vars, excludeFilters, binaryFilters) {
        return __awaiter(this, void 0, void 0, function () {
            var templatePath, files;
            var _this = this;
            return __generator(this, function (_a) {
                templatePath = path.resolve(srcPath);
                files = fs.readdirSync(templatePath, { withFileTypes: true });
                files.forEach(function (file) {
                    var extension = path.extname(file.name);
                    if (!extension) {
                        extension = file.name;
                    }
                    if (excludeFilters.indexOf(extension) === -1) {
                        var destPath = StringUtils.findAndReplace(path.join(outPath, file.name), vars);
                        if (file.isDirectory()) {
                            if (!fs.existsSync(destPath)) {
                                fs.mkdirSync(destPath);
                            }
                            _this.copyDirectory(path.join(templatePath, file.name), destPath, vars, excludeFilters, binaryFilters);
                        }
                        else if (binaryFilters.indexOf(extension) >= 0) {
                            _this.copyBinaryFile(path.join(templatePath, file.name), destPath, vars);
                        }
                        else {
                            _this.copyFile(path.join(templatePath, file.name), destPath, vars);
                        }
                    }
                });
                return [2 /*return*/];
            });
        });
    };
    ;
    return FileUtils;
}());
exports.default = FileUtils;
