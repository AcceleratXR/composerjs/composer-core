"use strict";
/**
 * Copyright (C) AcceleratXR, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Utility functions for sending push notifications to registered clients.
 *
 * @author Jean-Philippe Steinmetz
 */
var NotificationUtils = /** @class */ (function () {
    /**
     * Initializes the utility using the given configuration.
     *
     * @param {any} config The configuration to use for initialization.
     */
    function NotificationUtils(config) {
        this.socketio = null;
        this.socketio = require('socket.io-emitter')(config.redis).of(config.namespace);
    }
    /**
     * Broadcasts a given message to all users.
     *
     * @param {any} type The type of message being sent.
     * @param {any} message The message contents to send to all users.
     * @param {bool} volatile Set to true if the message can be dropped (unreliable).
     */
    NotificationUtils.prototype.broadcastMessage = function (type, message, volatile) {
        if (this.socketio) {
            if (volatile) {
                this.socketio.to('allusers').volatile.emit(type, message);
            }
            else {
                this.socketio.to('allusers').emit(type, message);
            }
        }
        else {
            throw new Error("Socket.io not configured or failed to connect.");
        }
    };
    /**
     * Sends a given message to the room or user with the specified uid.
     *
     * @param {string} uid The universally unique identifier of the room or user to send the message to.
     * @param {string} type The type of message being sent.
     * @param {string} message The message contents to send to the room or user.
     * @param {bool} volatile Set to true if the message can be dropped (unreliable).
     */
    NotificationUtils.prototype.sendMessage = function (uid, type, message, volatile) {
        if (this.socketio) {
            if (volatile) {
                this.socketio.to(uid).volatile.emit(type, message);
            }
            else {
                this.socketio.to(uid).emit(type, message);
            }
        }
        else {
            throw new Error("Socket.io not configured or failed to connect.");
        }
    };
    ;
    return NotificationUtils;
}());
exports.default = NotificationUtils;
