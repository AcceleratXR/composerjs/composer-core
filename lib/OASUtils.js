"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Copyright (C) AcceleratXR, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Author: Jean-Philippe Steinmetz
 * File: OASUtils.js
 * Purpose: Utility functions for working with files.
 */
var fs = require('fs');
var path = require('path');
var YAML = require('yamljs');
var logger_1 = __importDefault(require("./logger"));
var OASUtils = /** @class */ (function () {
    function OASUtils() {
    }
    /**
     * Gets the specification object at the specified path.
     *
     * @param {any} spec The OpenAPI specification to reference.
     * @param {string} path The path of the object to retrieve.
     * @returns {any} The object at the specified path if found, otherwise null.
     */
    OASUtils.prototype.getObject = function (spec, path) {
        var result = spec;
        if (path) {
            if (path[0] === '#') {
                path = path.substring(1, path.length);
            }
            if (path[0] === '/') {
                path = path.substring(1, path.length);
            }
            var parts = path.split('/');
            parts.forEach(function (part) {
                result = result[part];
            });
        }
        else {
            result = null;
        }
        return result;
    };
    /**
     * Returns the first available response content object for a 2XX response as defined by the provided Operation schema object.
     *
     * @param {any} obj The Operation schema object to search.
     */
    OASUtils.prototype.getResponseContent = function (obj) {
        if (obj.responses) {
            for (var status_1 in obj.responses) {
                if (status_1[0] === '2') {
                    for (var type in obj.responses[status_1]) {
                        return obj.responses[status_1].content;
                    }
                }
            }
        }
        return null;
    };
    /**
     * Retrieves the schema definition with the given name.
     *
     * @param {any} spec The OpenAPI specification object to reference.
     * @param {string} name The name of the schema to retrieve.
     * @returns {any} The schema definition with the given name.
     */
    OASUtils.prototype.getSchema = function (spec, name) {
        if (!spec.components || !spec.components.schemas) {
            throw new Error("Invalid specification. No schemas found.");
        }
        return spec.components.schemas[name];
    };
    /**
     * Extracts the type information for a given schema Object definition.
     *
     * @param {any} schemaDef The schema definition object to extract type information from.
     * @param {any} spec The entire OpenAPI specification object.
     * @param {Function} convertDataType The function that converts OpenAPI Specification types to native types.
     * @returns {any} A tuple containing the type, subType and subSchemaRef information.
     */
    OASUtils.prototype.getTypeInfo = function (schemaDef, spec, convertDataType) {
        if (schemaDef) {
            var result = { type: null, subSchemaRef: null, subType: null };
            if (schemaDef['$ref']) {
                var realDef = module.exports.getObject(spec, schemaDef['$ref']);
                if (realDef) {
                    result.subSchemaRef = schemaDef['$ref'];
                    result.type = convertDataType('object', null, path.basename(result.subSchemaRef));
                }
                else {
                    return null;
                }
            }
            else if (schemaDef['type'] === 'array') {
                if (schemaDef['items']) {
                    if (schemaDef.items['$ref']) {
                        var realDef = module.exports.getObject(spec, schemaDef.items['$ref']);
                        if (realDef) {
                            result.subSchemaRef = schemaDef.items['$ref'];
                            result.subType = convertDataType(realDef.type, realDef.format, path.basename(result.subSchemaRef));
                            result.type = convertDataType('array', result.subType);
                        }
                    }
                    else {
                        result.subType = convertDataType(schemaDef.items.type, schemaDef.items.format);
                        result.type = convertDataType('array', result.subType);
                    }
                }
                else {
                    throw new Error('Array defined with no items');
                }
            }
            else {
                result.type = convertDataType(schemaDef.type, schemaDef.format);
                if (schemaDef.type === 'object') {
                    result.subSchemaRef = 'Object';
                }
            }
            return result;
        }
        else {
            return null;
        }
    };
    /**
     * Attempts to load the Open API specification at the given path.
     *
     * @param {string} file The path of the OpenAPI Specification file to load.
     * @returns {any} The loaded OpenAPI Specification as an object.
     */
    OASUtils.prototype.loadSpec = function (file) {
        var apiSpec = null;
        if (fs.existsSync(file)) {
            var fileType = path.extname(file);
            if (fileType === '.yaml') {
                logger_1.default.info('Loading YAML: ' + file);
                apiSpec = YAML.load(file);
            }
            else if (fileType === '.json') {
                logger_1.default.info('Loading JSON: ' + file);
                apiSpec = require(file);
            }
            else {
                throw new Error("Unsupported file type: " + fileType);
            }
        }
        else {
            throw new Error("File not found: " + file);
        }
        return apiSpec;
    };
    return OASUtils;
}());
exports.default = OASUtils;
