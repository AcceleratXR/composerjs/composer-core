"use strict";
/**
 * Copyright (C) AcceleratXR, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Utility functions for working with strings.
 *
 * @author Jean-Philippe Steinmetz
 */
var StringUtils = /** @class */ (function () {
    function StringUtils() {
    }
    /**
     * Returns a list of all parameters contained within the string. A parameter is a bracket delimited substring
     * (e.g. /my/{key}/with/{id}).
     *
     * @param {any} str The string to search for parameters.
     * @returns {array} A list of parameters contained in the provided string.
     */
    StringUtils.prototype.getParameters = function (str) {
        var results = new Array();
        for (var i = 0; i < str.length; i++) {
            var start = str.indexOf('{', i);
            if (start !== -1) {
                var end = str.indexOf('}', start);
                if (end !== -1) {
                    results.push(str.substring(start + 1, end));
                    i = end;
                }
                else {
                    break;
                }
            }
            else {
                break;
            }
        }
        return results;
    };
    /**
     * Performs a search and replace on the provided contents with the map of variable replacements.
     *
     * @param {string} contents The stringt to perform the find and replace on.
     * @param {object} variables A map of key=>value pairs to search for and replace.
     */
    StringUtils.prototype.findAndReplace = function (contents, variables) {
        var output = contents;
        // Go through all variables and perform replacement
        for (var key in variables) {
            // Perform replacement on the variable value itself. This allows nested variable replacement.
            if (variables.has(key)) {
                var value = variables.get(key);
                for (var key2 in variables) {
                    if (variables.has(key2)) {
                        var value2 = variables.get(key2);
                        value = value.toString().replace(new RegExp('\\$' + key2, 'g'), value2);
                    }
                }
                output = output.replace(new RegExp('(\\$|%24)' + key, 'g'), value);
            }
        }
        return output;
    };
    ;
    /**
     * Finds and replaces all intances of the match regex pattern with the contents of innner regular expression pattern
     * in the given string.
     *
     * @param {string} str The string to perform replacement on.
     * @param {RegExp} match The regular expression pattern to match.
     * @param {string} replace The contents to replace the match filter with.
     * @param {string} prefix The prefix to prepend the replacement text with.
     * @returns {string} The fully replaced contents of the string.
     */
    StringUtils.prototype.replaceAll = function (str, match, prefix) {
        var result = str;
        var matches = str.match(match);
        while (matches) {
            result = result.replace(matches[0], prefix + matches[1]);
            matches = result.match(match);
        }
        return result;
    };
    /**
     * Converts the first character in the given string to be lowercase (e.g. myVariable).
     *
     * @param {string} str The string to convert to camelCase.
     * @returns {string} The string converted to camelCase.
     */
    StringUtils.prototype.toCamelCase = function (str) {
        return str.charAt(0).toLocaleLowerCase() + str.substring(1);
    };
    /**
     * Converts the first character in the given string to be uppercase (e.g. MyVariable).
     *
     * @param {string} str The string to convert to PascalCase.
     * @returns {string} The string converted to PascalCase.
     */
    StringUtils.prototype.toPascalCase = function (str) {
        return str.charAt(0).toLocaleUpperCase() + str.substring(1);
    };
    return StringUtils;
}());
exports.default = StringUtils;
