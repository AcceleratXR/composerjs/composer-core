"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Copyright (C) 2018 AcceleratXR, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var winston = require('winston');
var format = winston.format, transports = winston.transports;
var combine = format.combine, timestamp = format.timestamp, printf = format.printf;
var logFormat = printf(function (info) {
    return "[" + info.level + "] " + info.timestamp + ": " + info.message;
});
exports.default = winston.createLogger({
    transports: [
        new transports.Console()
    ],
    format: combine(format.splat(), format.simple(), format.colorize(), timestamp(), logFormat),
    level: 'debug'
});
