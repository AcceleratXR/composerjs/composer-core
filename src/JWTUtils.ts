///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import * as crypto from "crypto";
import * as jwt from "jsonwebtoken";
import * as zlib from "zlib";

/**
 * Describes the `User` object that is encoded in the payload of a JWT token.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export interface JWTUser {
    /**
     * The universally unique identifier of the user.
     */
    uid: string;

    /**
     * The date and time that the user was created.
     */
    dateCreated?: Date;

    /**
     * The date and time that the user was last modified.
     */
    dateModified?: Date;

    /**
     * The unique name of the user.
     */
    name?: string;

    /**
     * The unique e-mail address of the user.
     */
    email?: string;

    /**
     * The list of roles (by name) that the user is apart of and will assume privileges for.
     */
    roles?: string[];

    /**
     * The list of unique identifiers for each third-party platform the user is linked to.
     */
    externalIds?: string[];

    /**
     * Indicates if the user's e-mail address has been verified.
     */
    verified?: boolean;
}

/**
 * Describes the configuration options to be used with the `JWTUtilsConfig.payload` property.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export interface JWTUtilsPayloadOptions {
    /**
     * Set to `true` to enable payload compression, otherwise set to `false`.
     */
    compress: boolean;

    /**
     * Set to `true` to indicate that the JWT token payload is encrypted, otherwise set to `false`.
     */
    encrypt: boolean;
}

/**
 * Describes the configuration options to be used with the `JWTUtilsConfig.payload` property when performing password
 * based encryption.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export interface JWTUtilsPayloadPasswordOptions extends JWTUtilsPayloadOptions {
    /**
     * The cryptographic cipher algorithm to use during encryption/decryption of a JWT token payload.
     */
    algorithm: string;

    /**
     * The initialization vector to use during encryption and decryption.
     */
    iv: Buffer;

    /**
     * The password to use when encrypting or decrypting JWT token payloads.
     */
    password: string;
}

/**
 * Describes the configuration options to be used with the `JWTUtilsConfig.payload` property when performing
 * key-based encryption.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export interface JWTUtilsPayloadKeyOptions extends JWTUtilsPayloadOptions {
    /**
     * The private key used to encrypt JWT token payloads.
     */
    private_key: string;

    /**
     * The public key used to decrypt JWT token payloads.
     */
    public_key: string;
}

/**
 * Describes the configuration options to be used with `JWTUtils` functions.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export interface JWTUtilsConfig {
    /**
     * The password to use for signing and verifying JWT tokens.
     */
    password: string;

    /**
     * The options to use when performing JWT signing or verification.
     */
    options?: jwt.SignOptions | jwt.VerifyOptions;

    /**
     * The options that determine how JWT token payloads will be handled.
     */
    payload?: JWTUtilsPayloadOptions | JWTUtilsPayloadKeyOptions | JWTUtilsPayloadPasswordOptions;
}

/**
 * Utility class for working with Json Web Token (JWT) authentication tokens.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export class JWTUtils {
    /**
     * Generates a new JWT token for the given config and user object. The user object must be a valid AcceleratXR
     * user.
     *
     * @param config The JWT configuration to use when generating the token.
     * @param user The user to encode into the token's payload.
     */
    public static createToken(config: JWTUtilsConfig, user: JWTUser): string {
        // Validate the config options
        if (!config.password) {
            throw new Error("Invalid configuration provided.");
        }

        // Validate the user object
        if (!user || !user.uid) {
            throw new Error("Invalid or null user object provided.");
        }

        let payload: any = { profile: JSON.stringify(user) };

        // Compress the payload if desired
        if (config.payload && config.payload.compress) {
            const buf: Buffer = Buffer.from(payload.profile, "utf-8");
            payload.profile = zlib.gzipSync(buf).toString("base64");
        }

        // Encrypt the payload if desired
        if (config.payload && config.payload.encrypt) {
            const payloadOptions: any = config.payload;
            if (payloadOptions.private_key) {
                const keyOptions: JWTUtilsPayloadKeyOptions = payloadOptions as JWTUtilsPayloadKeyOptions;
                const encrypted: Buffer = crypto.privateEncrypt(keyOptions.private_key, Buffer.from(payload.profile));
                payload.profile = encrypted.toString("base64");
            } else {
                const pwOtions: JWTUtilsPayloadPasswordOptions = payloadOptions as JWTUtilsPayloadPasswordOptions;
                const iv: Buffer = Buffer.from(pwOtions.iv);
                const key: Buffer = crypto.scryptSync(pwOtions.password, "salt", 24);
                const cipher = crypto.createCipheriv(pwOtions.algorithm, key, iv);

                let encrypted: string = cipher.update(payload.profile, "utf8", "base64");
                encrypted += cipher.final("base64");
                payload.profile = encrypted;
            }
        }

        return jwt.sign(payload, config.password, config.options as jwt.SignOptions | undefined);
    }

    /**
     * Decodes the given JWT authentication token using the provided configuration. If the token is not valid an
     * error is thrown with the reason. Returns the encoded user object payload upon success.
     *
     * @param config The JWT configuration to use when validating the token.
     * @param token The JWT token to validate.
     * @returns The user object encoded in the token's payload.
     */
    public static decodeToken(config: JWTUtilsConfig, token: string): JWTUser {
        // Decode the token
        let payload: any = jwt.verify(token, config.password, config.options);

        // Validate the payload
        if (!payload || !payload.profile) {
            throw new Error("Token is invalid or missing data.");
        }

        // Decrypt the payload if desired
        if (config.payload && config.payload.encrypt) {
            const payloadOptions: any = config.payload;
            if (payloadOptions.private_key) {
                const keyOptions: JWTUtilsPayloadKeyOptions = payloadOptions as JWTUtilsPayloadKeyOptions;
                const decrypted: Buffer = crypto.privateDecrypt(keyOptions.private_key, Buffer.from(payload.profile));
                payload.profile = decrypted.toString("utf8");
            } else {
                const pwOtions: JWTUtilsPayloadPasswordOptions = payloadOptions as JWTUtilsPayloadPasswordOptions;
                const iv: Buffer = Buffer.from(payloadOptions.iv);
                const key: Buffer = crypto.scryptSync(payloadOptions.password as string, "salt", 24);
                const decipher = crypto.createDecipheriv(payloadOptions.algorithm, key, iv);

                let decrypted: string = decipher.update(payload.profile, "base64", "utf8");
                decrypted += decipher.final("utf8");
                payload.profile = decrypted;
            }
        }

        // Decompress the payload if desired
        if (config.payload && config.payload.compress) {
            const buf: Buffer = Buffer.from(payload.profile as string, "base64");
            payload.profile = zlib.gunzipSync(buf).toString("utf-8");
        }

        return JSON.parse(payload.profile);
    }
}
