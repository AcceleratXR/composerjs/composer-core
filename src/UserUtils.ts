///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////

/**
 * Utilities for working with authenticated user objects. An user object is expected to have the following
 * properties.
 *
 * * `uid` - Universally unique identifier for the user
 * * `email` - Unique e-mail address for the user
 * * `roles` - A list of unique names indicating the permissions of the user.
 * * `verified` - Indicates if the user's e-mail address has been verified.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export default class UserUtils {
    /**
     * Returns the unique identifier of the specified type associated with the given user.
     * 
     * @parm user The user to retrieve the external id from.
     * @param type The type of external provider to retrieve.
     * @returns The unique id of the external provider for the given type if found, otherwise `undefined`.
     */
    public static getExternalId(user: any, type: string): string | undefined {
        let result: string | undefined = undefined;

        if (user && Array.isArray(user.externalIds)) {
            for (const externalId of user.externalIds) {
                const parts: string[] = externalId.split(":");
                if (parts.length === 2 && parts[0] === type) {
                    result = parts[1];
                }
            }
        }

        return result;
    }

    /**
     * Returns `true` if the given user object has a role with the specified name, otherwise returns `false`.
     *
     * @param user The user object to inspect.
     * @param role The unique name of the role to search for.
     * @param orgUid The unique identifier of an organization whose role will be verified.
     */
    public static hasRole(user: any, role: string, orgUid?: string): boolean {
        let result: boolean = false;

        if (user) {
            if (user.roles) {
                if (Array.isArray(user.roles)) {
                    result = user.roles.includes(role);

                    if (!result && orgUid !== "") {
                        result = user.roles.includes(`${orgUid}.${role}`);
                    }
                }
            }
        }

        return result;
    }

    /**
     * Returns `true` if the given user object has at least one role from the specified list of names, otherwise returns `false`.
     *
     * @param user The user object to inspect.
     * @param roles A list of unique names of the roles to search for.
     * @param orgUid The unique identifier of an organization whose role will be verified.
     */
    public static hasRoles(user: any, roles: string[], orgUid?: string): boolean {
        if (user && user.roles && Array.isArray(user.roles)) {
            for (const role of roles) {
                if (UserUtils.hasRole(user, role, orgUid)) {
                    return true;
                }
            }
        }

        return false;
    }
}
