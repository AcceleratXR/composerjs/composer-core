///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { Worker } from "worker_threads";
const stripAnsi = require("strip-ansi");
const os = require("os");

/**
 * The `ThreadPool` class implements a method of managing a pool of execution threads that can be used for parallel
 * code execution. `Job`s are submitted to the pool and assigned to an available worker thread for execution.
 *
 * @author Jean-Philippe Steinmetz <info@acceleratxr.com>
 */
export default class ThreadPool {
    /** The map of event types to a list of callback functions. */
    private callbacks: Map<string, Array<Function>>;
    /** The index of the last worker that was assigned work. */
    private lastThread: number;
    /** The instance of Winston logger to forward thread logs to. */
    private logger: any;
    /** The maximum number of threads allowed. */
    private maxThreads: number;
    /** Indicates if a worker thread should automatically be restarted on exit. */
    private restartOnExit: boolean;
    /** The list of active worker threads. */
    public readonly workers: Array<Worker>;
    /** Used to indicate that the pool is shutting down. */
    private shutdown: boolean;

    /**
     * The maximum number of threads that can be created by the pool.
     */
    public get max(): number {
        return this.maxThreads;
    }

    /**
     * The number of active threads in the pool.
     */
    public get size(): number {
        return this.workers.length;
    }

    /**
     * Creates a new `ThreadPool` instance with the specified defaults.
     *
     * @param max The maximum number of threads to create. Default is `os.cpus().length`.
     * @param restartOnExit Set to `true` to restart a worker thread on exit, otherwise set to `false`. Default is `true`.
     * @param logger The Winston logger instance to forward all worker thread logs to.
     */
    constructor(max: number = 0, restartOnExit: boolean = true, logger?: any) {
        if (max <= 0) {
            max = os.cpus().length;
        }
        this.maxThreads = max;
        this.lastThread = 0;
        this.logger = logger;
        this.restartOnExit = restartOnExit;
        this.callbacks = new Map();
        this.shutdown = false;
        this.workers = new Array();
    }

    private createWorker(idx: number, script: string, args: any): Promise<Worker> {
        return new Promise((resolve, reject) => {
            const workerOptions: any = {
                workerData: args,
            };
            const worker: Worker = new Worker(script, workerOptions);

            worker.on("online", () => {
                resolve(worker);
            });
            worker.on("error", error => {
                const listeners: Array<Function> | undefined = this.callbacks.get("error");
                if (listeners) {
                    for (const callback of listeners) {
                        callback(error);
                    }
                }
            });
            worker.on("exit", async code => {
                const listeners: Array<Function> | undefined = this.callbacks.get("exit");
                if (listeners) {
                    for (const callback of listeners) {
                        callback(code);
                    }
                }

                // Restart worker thread
                if (this.restartOnExit && !this.shutdown) {
                    const worker: Worker = await this.createWorker(idx, script, args);
                    this.workers[idx] = worker;
                }
            });
            worker.on("message", (value) => {
                if (value && value.type === "winston-log") {
                    if (this.logger) {
                        value.info.level = stripAnsi(value.info.level);
                        this.logger.log(value.info);
                    }
                } else {
                    const listeners: Array<Function> | undefined = this.callbacks.get("message");
                    if (listeners) {
                        for (const callback of listeners) {
                            callback(value);
                        }
                    }
                }
            });
        });
    }

    /**
     * Initializes the thread pool with the initial worker threads and begins execution.
     *
     * @param script The script file to execute in the worker thread.
     * @param args The arguments to pass to the worker thread.
     * @param num The number of initial threads to create, cannot be greater than max. Default is `max`.
     */
    public start(script: string, args: any, num: number = this.max): Promise<void> {
        this.shutdown = false;

        return new Promise((resolve, reject) => {
            for (let i = 0; i < num && i < this.maxThreads; i++) {
                this.createWorker(i, script, args)
                    .then(worker => {
                        this.workers[i] = worker;

                        // When all workers have finished being created resolve
                        if (this.workers.length >= num || this.workers.length >= this.maxThreads) {
                            resolve();
                        }
                    })
                    .catch(err => {
                        reject(err);
                    });
            }
        });
    }

    /**
     * Stops all running thread executions.
     */
    public async stop(): Promise<void> {
        const listeners: Array<Function> | undefined = this.callbacks.get("exit");
        this.shutdown = true;

        for (const worker of this.workers) {
            const exitCode: number = await worker.terminate();
            if (listeners) {
                for (const callback of listeners) {
                    callback(exitCode);
                }
            }
        }
    }

    /**
     * Registers a new callback function to be notified when the given event type is fired.
     *
     * @param type The event type to be notified of. Possible values are: `error`, `exit` and `message`.
     * @param func The callback function to register.
     */
    public on(type: string, func: Function): void {
        let listeners: Array<Function> | undefined = this.callbacks.get(type);
        if (!listeners) {
            listeners = new Array();
        }
        listeners.push(func);
        this.callbacks.set(type, listeners);
    }

    /**
     * Sends the provided message to the next available worker thread. Messages are sent in a round-robin order.
     * @param args The message to send the next available worker thread.
     */
    public send(msg: any): void {
        const startIdx: number = this.lastThread;
        let worker: Worker | undefined = undefined;
        do {
            worker = this.workers[this.lastThread];
            this.lastThread = (this.lastThread + 1) % this.workers.length;
        } while (!worker && this.lastThread !== startIdx);

        worker.postMessage(msg);
    }

    /**
     * Sends the provided message to all worker threads in the pool.
     * @param msg  The message to send to all workers.
     */
    public sendAll(msg: any): void {
        for (const worker of this.workers) {
            worker.postMessage(msg);
        }
    }
}
