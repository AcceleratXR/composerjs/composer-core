///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
const Transport = require('winston-transport');
import { MessagePort } from "worker_threads";
const { MESSAGE } = require('triple-beam');

/**
 * Provides a Winston transport for forwarding logs from this thread to the parent's logger instance.
 * 
 * Constructor requires two properties to be set in the `opts` argument:
 * - `parentPort`
 * - `threadId`
 */
export default class ThreadWinstonTransport extends Transport {
    private parentPort: MessagePort;
    private threadId: number;

    /**
     * Creates a new instance with any defaults.
     * 
     * @param opts The default options to initialize with. Requires two properties, `parentPort` and `threadId`, that
     * must be provided from the thread worker script.
     */
    constructor(opts: any) {
        super(opts);

        this.parentPort = opts.parentPort;
        this.threadId = opts.threadId;
    }

    public log(info: any, callback: Function): void {
        setImmediate(() => this.emit('logged', info));

        // Prefix the thread ID to the message
        info[MESSAGE] = `[Thread-${this.threadId}]: ${info[MESSAGE]}`;

        // Forward the log to the parent thread
        if (this.parentPort) {
            this.parentPort.postMessage({
                type: "winston-log",
                info
            });
        }

        if (callback) {
            callback(); // eslint-disable-line callback-return
        }
    }
}