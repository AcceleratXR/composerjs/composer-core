///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
const path = require("path");
const { workerData } = require("worker_threads");

require("ts-node").register();

// In development we'll load the TypeScript source, in production we'll likely be running from transpiled JavaScript
const tsFile = path.resolve(workerData.script + ".ts");
if (fs.existsSync(tsFile)) {
    require(tsFile);
} else {
    require(path.resolve(workerData.script + ".js"));
}
