///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { StringUtils } from "../src/core";
import { stringify } from "querystring";

describe("StringUtils Tests.", () => {
    it("getParameters returns successfully.", () => {
        var result = StringUtils.getParameters("/my/{key}/with/{id}");
        expect(result).toBeInstanceOf(Array);
        expect(result).toHaveLength(2);
        expect(result[0]).toBe("key");
        expect(result[1]).toBe("id");
    });

    it("findAndReplace returns successfully.", () => {
        var map = {
            adjective: "lazy",
            animal1: "Fox",
            animal2: "{{adjective}} Dog",
            color: "brown",
        };
        var result = StringUtils.findAndReplace("The quick {{color}} {{animal1}} jumped over the {{animal2}}.", map);
        expect(result).toBe("The quick brown Fox jumped over the lazy Dog.");
    });

    it("replaceAll returns successfully.", () => {
        var result = StringUtils.replaceAll("/my/{uid}/child/{childid}", new RegExp("\\{([^\\}]+)\\}"), ":");
        expect(result).toBe("/my/:uid/child/:childid");
    });

    it("toCamelCase returns successfully.", () => {
        expect(StringUtils.toCamelCase("myString")).toBe("myString");
        expect(StringUtils.toCamelCase("MyString")).toBe("myString");
        expect(StringUtils.toCamelCase("MYSTRING")).toBe("mYSTRING");
    });

    it("toPascalCase returns successfully.", () => {
        expect(StringUtils.toPascalCase("myString")).toBe("MyString");
        expect(StringUtils.toPascalCase("MyString")).toBe("MyString");
        expect(StringUtils.toPascalCase("MYSTRING")).toBe("MYSTRING");
        expect(StringUtils.toPascalCase("mystring")).toBe("Mystring");
    });
});
