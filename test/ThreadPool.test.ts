///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { ThreadPool } from "../src/threads";
const os = require("os");

describe("ThreadPool Tests.", () => {
    it("Can create thread pool with 1 thread.", async (done: Function) => {
        const pool: ThreadPool = new ThreadPool(1, false);
        expect(pool).toBeDefined();

        let error: any = undefined;
        pool.on("error", (err: any) => {
            error = err;
        });

        let exitCode: number = -1;
        pool.on("exit", (code: number) => {
            exitCode = code;
        });

        let sum: number = 0;
        pool.on("message", (result: number) => {
            sum += result;
        });

        await pool.start("./test/ThreadSimpleJob.js", 10);

        setTimeout(() => {
            expect(error).toBeUndefined();
            expect(exitCode).toBeLessThanOrEqual(0);
            expect(sum).toBe(10 * 2);
            done();
        }, 100);
    });

    it("Can create thread pool with n threads.", async (done: Function) => {
        const pool: ThreadPool = new ThreadPool(os.cpus().length, false);
        expect(pool).toBeDefined();

        let error: any = undefined;
        pool.on("error", (err: any) => {
            error = err;
        });

        let exitCode: number = -1;
        pool.on("exit", (code: number) => {
            exitCode = code;
        });

        let sum: number = 0;
        pool.on("message", (result: number) => {
            sum += result;
        });

        await pool.start("./test/ThreadSimpleJob.js", 10);
        setTimeout(() => {
            expect(error).toBeUndefined();
            expect(exitCode).toBeLessThanOrEqual(0);
            expect(sum).toBe(10 * 2 * os.cpus().length);
            done();
        }, 100);
    });

    it("Can keep recreating threads.", async (done: Function) => {
        const pool: ThreadPool = new ThreadPool();
        expect(pool).toBeDefined();

        let error: any = undefined;
        pool.on("error", (err: any) => {
            error = err;
        });

        let exitCode: number = -1;
        pool.on("exit", (code: number) => {
            exitCode = code;
        });

        let sum: number = 0;
        pool.on("message", (result: number) => {
            sum += result;
        });

        await pool.start("./test/ThreadSimpleJob.js", 10);

        setTimeout(async () => {
            expect(error).toBeUndefined();
            expect(exitCode).toBeLessThanOrEqual(0);
            expect(sum).toBeGreaterThanOrEqual(10 * 2 * os.cpus().length);
            await pool.stop();
            done();
        }, 1000);
    });
});
