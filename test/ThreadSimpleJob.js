///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
const { parentPort, workerData } = require("worker_threads");

parentPort.postMessage(workerData * 2);
