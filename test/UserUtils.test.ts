///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
import { UserUtils } from "../src/core";
import * as uuid from "uuid";

describe("UserUtils Tests.", () => {
    const orgUid: string = uuid.v4();
    const testUser = {
        uid: uuid.v4(),
        email: "test@gmail.com",
        roles: ["role1", "role2", `${orgUid}.role3`, `${orgUid}.role4`],
        personas: [{ uid: uuid.v4() }, { uid: uuid.v4() }, { uid: uuid.v4() }],
        externalIds: [`facebook:89761181`, `twitter:567896132`],
        orgs: [orgUid],
    };

    it("Can get external id.", () => {
        expect(UserUtils.getExternalId(testUser, "facebook")).toBe("89761181");
        expect(UserUtils.getExternalId(testUser, "twitter")).toBe("567896132");
        expect(UserUtils.getExternalId(testUser, "google")).toBeUndefined();
    });

    it("Can check for single role.", () => {
        expect(UserUtils.hasRole(testUser, "role1")).toBe(true);
        expect(UserUtils.hasRole(testUser, "role2")).toBe(true);
        expect(UserUtils.hasRole(testUser, "role3")).toBe(false);
        expect(UserUtils.hasRole(testUser, "role4")).toBe(false);
    });

    it("Can check for multiple roles.", () => {
        expect(UserUtils.hasRoles(testUser, ["role1", "role2"])).toBe(true);
        expect(UserUtils.hasRoles(testUser, ["role1", "role3"])).toBe(true);
        expect(UserUtils.hasRoles(testUser, ["role4", "role2"])).toBe(true);
        expect(UserUtils.hasRoles(testUser, [])).toBe(false);
        expect(UserUtils.hasRoles(testUser, ["role3", "role4"])).toBe(false);
    });
});
