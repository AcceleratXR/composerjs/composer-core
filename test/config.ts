///////////////////////////////////////////////////////////////////////////////
// Copyright (C) AcceleratXR, Inc. All rights reserved.
///////////////////////////////////////////////////////////////////////////////
const conf = require("nconf")
    .argv()
    .env({
        separator: "__",
        parseValues: true,
    });

conf.defaults({
    jobs: {
        defaultSchedule: "*/2 * * * * *",
        MyFirstService: {
            schedule: "* * * * * *",
        },
        MySecondService: {
            schedule: "* * * * * *",
        },
        MyThirdService: {},
    },
    auth: {
        password: "MyPasswordIsSecure",
        options: {
            audience: "composerjs.com",
            issuer: "composerjs.com",
        },
    }
});

export default conf;
